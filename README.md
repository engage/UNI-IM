# UNI-IM

#### 项目介绍
采用[UNI] + [5+] 通过简单配置即可集成 即时通讯功能

DOME 可直接真机运行


#### 安装教程
配置 请参考Dome

1. 会话配置
~~~
{
	info:{
		// 用户关键字
		userKey:2666,
		
		// 用户手机
		userPhone:15252156614,
		
		// 用户昵称
		userName: '健健',
		
		// 头像
		headImg: 'http://d.hiphotos.baidu.com/image/h%3D300/sign=0defb42225381f3081198ba999004c67/6159252dd42a2834a75bb01156b5c9ea15cebf2f.jpg',
		
		// [选填] token
		token:'123123'
	},
	
	// 项目配置 *[自行申请 替换就OK]
	ProjectInfo: {
		userPhone: "15252156614",
		userToken: "0ZJ_A5F0-F4AC1DD644DA",
		projectToken: "CC8-A829-E530731C0FD6",
		callUrl: "http://socketplus.ecjsw.com/MsgCall/tack"
	},
	
	style:{
		// 窗口颜色 配置
		nTitle:{
			color: '#ffffff',
			background: '#1482d1'
		}
	}
}
~~~

2. 聊天列表配置

~~~
	{
			
		// 会话名称
		title:item.title,
			
		// 对方 UserKey [type long] == 会话.info.userKey
		toUserKey:item.mobile,
			
		// 头像 自定义
		headImg: 'https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=3400163923,2373757593&fm=173&app=25&f=JPEG?w=550&h=413&s=F29B1DC74EA34E96EB11E9DF03009097',
			
		// [0|1]可选 0点对点，1群组会话
		sessionType:0 
	}
~~~


#### 使用说明

[前往获取注册信息](https://www.help-itool.com/)