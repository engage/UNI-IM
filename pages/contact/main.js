let main = plus.android.runtimeMainActivity();

//为了防止快速点按返回键导致程序退出重写quit方法改为隐藏至后台
plus.runtime.quit = function () {
	main.moveTaskToBack(false);
};

//重写toast方法如果内容为 ‘再按一次退出应用’ 就隐藏应用，其他正常toast
plus.nativeUI.toast = (function (str) {
	if (str == '再按一次退出应用') {
		main.moveTaskToBack(false);
		return false;
	} else {
		uni.showToast({
			title: str,
			icon: 'none',
		})
	}
});


export default {

	getContacks: function (cb) {

		plus.contacts.getAddressBook(plus.contacts.ADDRESSBOOK_PHONE, function (addressbook) {

			addressbook.find(["displayName", "phoneNumbers"], function (contacts) {

				var contacta_list = [];

				contacts.forEach(function (item, index) {

					if (!item.phoneNumbers.length) return;

					var mobile = (item.phoneNumbers[0].value + '').replace(/\s/g, '').replace('+86', '').replace(/-/g, '').replace(
						'17951', '');

					if (mobile.length != 11 || !item.displayName) return;

					contacta_list.push({
						mobile: mobile,
						title: item.displayName
					});
				});

				return cb && cb(contacta_list);
			});
		});
	}
}
