export default {
	// 滚动到底部
	srollToBottom: function(that) {
		var query = uni.createSelectorQuery();
		query.selectAll('.m-item').boundingClientRect();
		query.select('#scrollview').boundingClientRect();

		query.exec(function (res) {
			that.style.mitemHeight = 0;
			res[0].forEach(function (rect) {
				// console.info(rect.height);
				that.style.mitemHeight = that.style.mitemHeight + rect.height + 120;
			});

			if (that.style.mitemHeight > that.style.contentViewHeight) {
				that.scrollTop = that.style.mitemHeight - that.style.contentViewHeight;
			}
		});
	},

	// 输入框高度
	refreshContentHeigth: function(that) {
		const res = uni.getSystemInfoSync();
		that.style.pageHeight = res.windowHeight;
		that.style.contentViewHeight = res.windowHeight - uni.getSystemInfoSync().screenWidth / 750 * (100); //像素
	}
}
